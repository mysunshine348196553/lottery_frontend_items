import React, {Component} from 'react';
import "./main.css";
import {dataRequest} from "../api";
import classNames from "classnames";
import {
    message,List,Button,Modal,Checkbox, Row, Col
} from 'antd';

class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {"total": 0, "data": [],visible: false,modelVisible:false,"a":[],"b":[],"c":[],"d":[],"e":[],"list":[]};
    }

    refresh=()=>{
        console.log("refresh");
        this.fetchTicketList(100);
    };

    fetchTicketList = (total) => {
        const that = this;
        dataRequest(total)
            .then(function (response) {
                const data = response.data;
                if (data.code === "0") {
                    message.success("数据获取成功");
                    that.setState({"total": data.total,combineList:[], "data": data.data,"a":data.a,"b":data.b,"c":data.c,"d":data.d,"e":data.e});
                }
                else {
                    message.error(data.msg);
                }
            })
            .catch(function (error) {
                message.error(error.message);
            });
    };


    handleOk = (e) => {
        console.log(e);
        this.setState({
            visible: false,
        });
    };
    showModal=(modal,event)=>{
        this.setState({
            visible: true,
            "list":this.state[modal],
        });

    };
    handleCombineOk = (e) => {
        console.log(e);
        this.setState({
            modelVisible: false,
        });
    };
    showCombine=()=>this.setState({modelVisible:true});
    onCheckChange=(checkedValues)=> {
        console.log('checked = ', checkedValues);
        let that=this;
        const arr=[];
        for(let i=0;i<100;i++)
        {
            let index=i<10?"0"+i:i+"";
            let isOut=false;
            for(let value in checkedValues)
            {
                if((that.state[checkedValues[value]].indexOf(index))===-1)
                {
                    //不包含该元素
                    isOut=true;
                    break;
                }
            }
            if(!isOut)
            {
                arr.push(index);
            }
        }
        that.setState({combineList:arr});
        console.log(arr);
    };

    componentDidMount() {
        console.log("componentDidMount");
        this.fetchTicketList(100);
    }

    render() {
        return (<div className={"main"}>
                <Modal
                    title="合并列表"
                    maskClosable
                    visible={this.state.modelVisible}
                    onOk={this.handleCombineOk}
                    onCancel={this.handleCombineOk}
                >
                    <Checkbox.Group style={{ width: '100%' }} onChange={this.onCheckChange}>
                        <Row>
                            <Col span={8}><Checkbox value="a">A组</Checkbox></Col>
                            <Col span={8}><Checkbox value="b">B组</Checkbox></Col>
                            <Col span={8}><Checkbox value="c">C组</Checkbox></Col>
                            <Col span={8}><Checkbox value="d">D组</Checkbox></Col>
                            <Col span={8}><Checkbox value="e">E组</Checkbox></Col>
                        </Row>
                    </Checkbox.Group>
                    <List bordered
                          className={"modal"}
                          dataSource={this.state.combineList}
                          renderItem={item=>(
                              <List.Item>{<div>{item}</div>}</List.Item>
                          )}
                    />
                </Modal>
                <Modal

                    title="号码列表"
                    maskClosable
                    visible={this.state.visible}
                    onOk={this.handleOk}
                    onCancel={this.handleOk}
                >
                   <List bordered
                         className={"modal"}
                         dataSource={this.state.list}
                         renderItem={item=>(
                             <List.Item>{<div>{item}</div>}</List.Item>
                         )}
                   />
                </Modal>
                <div className={"list-item"}>
                    <Button type="primary" onClick={this.refresh}>刷新</Button>
                    <Button type="primary" className={"btn"} onClick={this.showCombine}>合并号码</Button>
                    <Button type="primary" className={"btn"} onClick={this.showModal.bind(this,"a")}>A组号码</Button>
                    <Button type="primary" className={"btn"} onClick={this.showModal.bind(this,"b")}>B组号码</Button>
                    <Button type="primary" className={"btn"} onClick={this.showModal.bind(this,"c")}>C组号码</Button>
                    <Button type="primary" className={"btn"} onClick={this.showModal.bind(this,"d")}>D组号码</Button>
                    <Button type="primary" className={"btn"} onClick={this.showModal.bind(this,"e")}>E组号码</Button>
                </div>

                <List header={<div className={"list-item"}>
                    <div className={"lotteryNo"}>彩票期数</div>
                    <div className={"lotteryNums"}>中奖号码</div>
                    <div className={"date"}>开奖日期</div>
                    <div className={"win"}>A组</div>
                    <div className={"win"}>B组</div>
                    <div className={"win"}>C组</div>
                    <div className={"win"}>D组</div>
                    <div className={"win"}>E组</div>
                </div>}
                bordered
                dataSource={this.state.data}
                renderItem={item=>(
                    <List.Item>{<div className={"list-item"}>
                        <div className={"lotteryNo"}>{item.lotteryNo}</div>
                        <div className={"lotteryNums"}>{item.lotteryNums}</div>
                        <div className={"date"}>{item.date}</div>
                        <div className={classNames({
                            "a":true,
                            "b":item.aGroup

                        })}></div>
                        <div className={classNames({
                            "a":true,
                            "b":item.bGroup
                        })}></div>
                        <div className={classNames({
                            "a":true,
                            "b":item.cGroup

                        })}></div>
                        <div className={classNames({
                            "a":true,
                            "b":item.dGroup

                        })}></div>
                        <div className={classNames({
                            "a":true,
                            "b":item.eGroup
                        })}></div>
                    </div>}</List.Item>
                    )}
                />
        </div>
        );
    }
}

export default Main;
