import axios from 'axios';
//const baseUrl="http://112.74.187.145:8080/user";
const baseUrl="http://112.74.187.145:9090/api";

axios.interceptors.response.use(response => {
    //对响应数据做操作
    if(response.data.code==="4001") {
        //登录失效问题
        window.location.href = '/';
        return response
    }
    return response;

}, error => {
    //对响应数据错误做操作
    console.log('请求error', error.message);
    return Promise.reject(error);
});

export const dataRequest=(value)=>{
   return axios.get(baseUrl+"/selectTicket?num="+value);
};


export const loginRequest=(values)=>{
    return axios.get(baseUrl+"/login?username="+values.userName+"&password="+values.password);
};