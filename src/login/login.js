import React, { Component } from 'react';
import './index.css';
import {
    Form, Icon, Input, Button,message,
} from 'antd';
import {loginRequest} from "../api";

class Login extends Component {
    handleSubmit = (e) => {
        e.preventDefault();
        const that=this;
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
                loginRequest(values)
                    .then(function (response) {
                        const data=response.data;
                        console.log('Received values of form: ', data);
                       // if(response.)
                        if(data.code==="0") {
                            message.success("登录成功");
                            that.props.history.replace('/main');
                        }
                        else{
                            message.error(data.msg);
                        }
                    })
                    .catch(function (error) {
                        message.error(error.message);
                    });
            }
        });
    };
    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <div className={"app"}>
                <Form onSubmit={this.handleSubmit} className="login-form">
                    <Form.Item>
                        {getFieldDecorator('userName', {
                            rules: [{ required: true, message: '请输入你的账号!' }],
                        })(
                            <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="账号" />
                        )}
                    </Form.Item>
                    <Form.Item>
                        {getFieldDecorator('password', {
                            rules: [{ required: true, message: '请输入你的密码!' }],
                        })(
                            <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="密码" />
                        )}
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" htmlType="submit" className="login-form-button">
                            登录
                        </Button>
                    </Form.Item>
                </Form>
            </div>

        );
    }
}
const LoginComponent = Form.create()(Login);
export default LoginComponent;
